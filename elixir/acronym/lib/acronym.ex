defmodule Acronym do

  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(String.t()) :: String.t()
  def abbreviate(string) when is_binary(string) do
    string
    |> String.replace(~R/[A-Z]/, " \\0")
    |> String.split(~R/[^[:alnum:]\-]/u, trim: true)
    |> Enum.reduce("", fn(w, acc) ->
      letter = w |> String.at(0) |> String.upcase
      acc <> letter
    end)
  end

end
