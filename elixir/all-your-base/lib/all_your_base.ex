defmodule AllYourBase do
  @doc """
  Given a number in base a, represented as a sequence of digits, converts it to base b,
  or returns nil if either of the bases are less than 2
  """

  @spec convert(list, integer, integer) :: list

  def convert(digits, base_a, base_b)
  when digits === []
  when base_a <= 1
  when base_b <= 1 , do: nil

  def convert(digits, base_a, base_b) do

    case invalid?(digits, base_a) do
      false -> process(digits, base_a, base_b)
      true -> nil
    end
  end

  ## A function that check for invalid parameter
  defp invalid?(digits, base_a) do
    digits |> Enum.any?(&(&1 >= base_a || &1 < 0))
  end

  ## Simple function to test if there only 0
  defp zero?(digits) do
    digits |> Enum.all?(&(&1 == 0))
  end

  ## This function is a simple wrapper for more clarity
  defp process(digits, base_a, base_b) do
    data = digits |> to_base(base_a) |> from_base(base_b)

    case zero?(data) do
      true -> [0]
      _ -> data
    end
  end

  ## Get the number in the root base
  defp to_base(digits, base) do
    digits
    |> Enum.reduce(0, fn v, acc ->
      acc * base + v
    end)
  end

  ## This function will separate the number into list of the new base
  defp from_base(0, _), do: []

  defp from_base(digits, base) do
    ret = div(digits, base)

    from_base(ret, base) ++ [rem(digits, base)]
  end

end
