defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """

  @spec match(String.t(), [String.t()]) :: [String.t()]
  @spec compare(String.t, String.t) :: Boolean.t()
  @spec format(String.t) :: String.t

  def match(base, candidates) do
    Enum.filter(candidates, &(compare(base, &1)))
  end

  def compare(base, candidate) do
    base = String.downcase(base)
    candidate = String.downcase(candidate)

    base != candidate and format(base) == format(candidate)
  end

  def format(str) do
    str
    |> String.codepoints
    |> Enum.sort
  end
end
