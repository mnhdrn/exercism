defmodule BinarySearch do
  @doc """
    Searches for a key in the tuple using the binary search algorithm.
    It returns :not_found if the key is not in the tuple.
    Otherwise returns {:ok, index}.

    ## Examples

      iex> BinarySearch.search({}, 2)
      :not_found

      iex> BinarySearch.search({1, 3, 5}, 2)
      :not_found

      iex> BinarySearch.search({1, 3, 5}, 5)
      {:ok, 2}

  """

  @spec search(tuple, integer) :: {:ok, integer} | :not_found
  def search({}, _), do: :not_found

  def search(numbers, key) do
    numbers
    |> Tuple.to_list
    |> bs_search(key, 0)
    |> case do
      :not_found -> :not_found
      n -> {:ok, n}
    end
  end

  @spec bs_search(list, integer, integer) :: integer | :not_found
  def bs_search([], _, _), do: :not_found

  def bs_search(lst, key, acc) do
    len = length(lst)
    mid = div(len, 2)
    el = Enum.at(lst, mid)

    cond do
      el == key -> acc + mid
      el > key -> bs_search(Enum.slice(lst, 0..mid - 1), key, acc)
      el < key -> bs_search(Enum.slice(lst, mid + 1..len), key, (acc + mid + 1))
    end
  end
end
