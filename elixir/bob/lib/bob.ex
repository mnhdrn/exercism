defmodule Bob do

  def hey(input) do
    cond do
      silence?(input) -> "Fine. Be that way!"
      question?(input) and yelling?(input) -> "Calm down, I know what I'm doing!"
      question?(input) -> "Sure."
      yelling?(input) -> "Whoa, chill out!"
      true -> "Whatever."
    end
  end

  defp silence?(input) when is_binary(input) do
    String.trim(input) == ""
  end

  defp question?(input) when is_binary(input) do
    String.ends_with?(input, "?")
  end

  defp yelling?(input) when is_binary(input) do
    (String.downcase(input) != input) and (String.upcase(input) == input)
  end

end
