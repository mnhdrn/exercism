defmodule Bowling do

  defguard is_strike(score) when is_integer(score) and score == 10

  defguardp is_frame_start(roll) when rem(roll, 2) == 0

  defguard is_last_frame(roll_count) when is_integer(roll_count) and roll_count > 18 and roll_count < 21

  defguard is_pin_overflow(a, b) when is_integer(a) and is_integer(b) and a + b > 10

  # ===================================================================================#
  # -----------------------------------------------------------------------------------#
  # Game Structure

  defmodule Game do
    defstruct roll_points: [], rolls: 0
  end

  # ===================================================================================#
  # -----------------------------------------------------------------------------------#
  # start

  @spec start() :: any
  @doc """
  Creates a new game of bowling that can be used to store the results of
  the game
  """

  def start do
    %Game{}
  end

  # ===================================================================================#
  # -----------------------------------------------------------------------------------#
  # roll

  @spec roll(any, integer) :: any | String.t()
  @doc """
  Records the number of pins knocked down on a single roll. Returns `any`
  unless there is something wrong with the given number of pins, in which
  case it returns a helpful message.
  """

  def roll(_, roll) when roll < 0, do: {:error, "Negative roll is invalid"}

  def roll(_, roll) when roll > 10, do: {:error, "Pin count exceeds pins on the lane"}

  ### INIT ROLL

  # Strike
  def roll(%Game{roll_points: [], rolls: 0}, 10) do
    %Game{roll_points: [10, 0], rolls: 2}
  end

  # Regular
  def roll(%Game{roll_points: [], rolls: 0}, roll) do
    %Game{roll_points: [roll], rolls: 1}
  end

  ### LAST FRAME ROLL PROTECTION

  def roll(%Game{roll_points: [hd | [sd | _td]], rolls: rolls}, _roll)
  when rolls == 20
  and not is_strike(sd)
  and not is_strike(hd + sd) do
    {:error, "Cannot roll after game is over"}
  end

  def roll(%Game{roll_points: [hd | [sd | _td]], rolls: rolls}, roll)
  when rolls == 20
  and is_pin_overflow(hd, roll)
  and not is_strike(hd + sd) do
    {:error, "Pin count exceeds pins on the lane"}
  end

  ### STRIKE ROLL

  # Normal Frame Strike
  def roll(%Game{roll_points: roll_points, rolls: rolls}, 10)
  when rolls < 18 do
    %Game{roll_points: [10, 0] ++ roll_points, rolls: rolls + 2}
  end

  # Last Frame Strike
  def roll(%Game{roll_points: roll_points, rolls: rolls}, 10) do
    %Game{roll_points: [10, 0] ++ roll_points, rolls: rolls + 1}
  end

  ### NORMAL ROLL

  # Regular Roll Protection
  def roll(%Game{roll_points: [hd | _td], rolls: rolls}, roll)
  when not is_frame_start(rolls)
  and not is_strike(hd)
  and is_pin_overflow(hd, roll) do
    {:error, "Pin count exceeds pins on the lane"}
  end

  # Regular
  def roll(%Game{roll_points: roll_points, rolls: rolls}, roll) do
    %Game{roll_points: [roll] ++ roll_points, rolls: rolls + 1}
  end

  # ===================================================================================#
  # -----------------------------------------------------------------------------------#
  # score

  @spec score(any) :: integer | String.t()
  @doc """
    Returns the score of a given game of bowling if the game is complete.
    If the game isn't complete, it returns a helpful message.
  """

  ### SCORE PROTECTION

  def score(%Game{roll_points: []}), do: {:error, "Score cannot be taken until the end of the game"}

  def score(%Game{roll_points: [hd | [sd | _td]], rolls: rolls})
  when is_strike(hd + sd)
  and is_last_frame(rolls) do
    {:error, "Score cannot be taken until the end of the game"}
  end

  def score(%Game{roll_points: [hd | _td], rolls: rolls})
  when is_strike(hd)
  and is_last_frame(rolls) do
    {:error, "Score cannot be taken until the end of the game"}
  end

  def score(%Game{rolls: rolls})
  when rolls < 20 do
    {:error, "Score cannot be taken until the end of the game"}
  end

  ### SCORE

  def score(%Game{roll_points: roll_points}) do
    roll_points
    |> Enum.reverse()
    |> Enum.chunk_every(2)
    |> count_point(0)
  end

  # ===================================================================================#
  # -----------------------------------------------------------------------------------#
  # count_point

  @spec count_point(List.t(), Integer.t()) :: Integer.t()
  @doc """
  Take an array with subarray, and reduce them to an int, to get the final score.
  """

  def count_point([], acc), do: acc

  ### FINAL FRAME STRIKE

  # Double Strike
  def count_point([[0, 10], [0, 10] | [sd | []]], acc) do
    acc + 20 + Enum.sum(sd)
  end

  # Simple Strike
  def count_point([[0, 10] | [sd | []]], acc) do
    acc + 10 + Enum.sum(sd)
  end

  ### REGULAR FRAME

  # Double Strike
  def count_point([[0, 10] | [[0, 10] | [sd | td]]], acc) do
    c =
      case sd do
        [0, 10] -> 10
        n -> n |> Enum.at(0)
      end

    count_point([[0, 10] | [sd | td]], acc + 20 + c)
  end

  # Simple Strike
  def count_point([[0, 10] | [sd | td]], acc) do
    c = sd |> Enum.sum()

    count_point([sd | td], acc + 10 + c)
  end

  # Spare
  def count_point([[a, b] | [sd | td]], acc)
  when is_strike(a + b) do
    c = sd |> Enum.at(0)

    count_point([sd | td], acc + a + b + c)
  end

  # Open Frame
  def count_point([[a, b] | td], acc) do
    count_point(td, acc + a + b)
  end

  ### END CASE

  def count_point([_hd | []], acc), do: acc
end
