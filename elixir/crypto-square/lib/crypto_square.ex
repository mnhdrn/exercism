defmodule CryptoSquare do
  @doc """
  Encode string square methods
  ## Examples

    iex> CryptoSquare.encode("abcd")
    "ac bd"
  """
  @spec encode(String.t()) :: String.t()
  def encode(""), do: ""

  def encode(str) when is_binary(str) do
    str = str |> normalize
    size = get_size(str)
    leftover = String.duplicate(" ", size) |> String.graphemes

    str
    |> String.graphemes
    |> Enum.chunk_every(size, size, leftover)
    |> Enum.zip
    |> Enum.map(fn a -> Tuple.to_list(a) |> Enum.join |> normalize end) # removing extra space to fill chunk_every.
    |> Enum.join(" ")
  end

  defp normalize(str) do
    str
    |> String.downcase
    |> String.replace(~r/[^[:alnum:]]/, "")
  end

  defp get_size(str) do
    str
    |> String.length
    |> :math.sqrt
    |> Float.ceil
    |> trunc
  end
end
