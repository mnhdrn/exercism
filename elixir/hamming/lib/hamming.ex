defmodule Hamming do
  @doc """
  Returns number of differences between two strands of DNA, known as the Hamming Distance.

  ## Examples

  iex> Hamming.hamming_distance('AAGTCATA', 'TAGCGATC')
  {:ok, 4}
  """
  @spec hamming_distance([char], [char]) :: {:ok, non_neg_integer} | {:error, String.t()}
  def hamming_distance(strand1, strand2) 
  when length(strand1) == length(strand2)
  do
    [strand1, strand2]
    |> List.zip
    |> Enum.reduce({:ok, 0}, &(count(&1, &2)))
  end

  def hamming_distance(_, _), do: {:error, "Lists must be the same length"}

  defp count({x, y}, {key, value}) do
    if x == y do
      {key, value}
    else
      {key, value + 1}
    end
  end

end

IO.inspect Hamming.hamming_distance('TAG', 'GAT')

