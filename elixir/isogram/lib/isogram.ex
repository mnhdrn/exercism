defmodule Isogram do

  @alphabet ?a..?z

  @doc """
  Determines if a word or sentence is an isogram
  """
  @spec isogram?(String.t()) :: boolean
  def isogram?(sentence) do
    data = sentence
    |> String.downcase
    |> String.replace(~r/-|\s/u, "")
    |> String.graphemes
    |> Enum.sort
    |> Enum.chunk_by(&(&1))
    |> Enum.map(&(Enum.count(&1)))
    |> Enum.all?(&(&1 < 2))
  end

end
