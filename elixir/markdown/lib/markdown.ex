defmodule Markdown do
  @doc """
    Parses a given string with Markdown syntax and returns the associated HTML for that string.

    ## Examples

    iex> Markdown.parse("This is a paragraph")
    "<p>This is a paragraph</p>"

    iex> Markdown.parse("#Header!\n* __Bold Item__\n* _Italic Item_")
    "<h1>Header!</h1><ul><li><em>Bold Item</em></li><li><i>Italic Item</i></li></ul>"
  """
  @spec parse(String.t()) :: String.t()

  ## Just using pipes for a better readibility

  def parse(str) do
    str
    |> String.split("\n")
    |> Enum.map_join("", &process/1)
    |> unordered_tag
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Using pipes and guard to avoid nested 'if'
  # Also spliting before sending so I can 
  # use pattern matching later

  defp process("#" <> _rest = t) do
    t
    |> String.split()
    |> parse_header_md_level
    |> enclose_with_header_tag
  end

  defp process("*" <> _rest = t) do
    t |> parse_list_md_level
  end

  defp process(t) do
    t
    |> String.split()
    |> enclose_with_paragraph_tag
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Using pattern matching for more readibility
  # Also piping in variable.

  defp parse_header_md_level([hd | td]) do
    head = hd |> String.length() |> to_string
    tail = td |> Enum.join(" ")

    {head, tail}
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Just piping here

  defp parse_list_md_level(l) do
    t =
      l
      |> String.trim_leading("* ")
      |> String.split()

    "<li>" <> join_words_with_tags(t) <> "</li>"
  end

  # =============================================================
  # -------------------------------------------------------------
  ##  I think this one is clear enought, I just change variable name

  defp enclose_with_header_tag({hl, content}) do
    "<h" <> hl <> ">" <> content <> "</h" <> hl <> ">"
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Replacing '#{}' in string to having more consistency between
  # function

  defp enclose_with_paragraph_tag(t) do
    "<p>" <> join_words_with_tags(t) <> "</p>"
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Using map join to reduce the number of call
  # also using anonyme function with &(&1) technique

  defp join_words_with_tags(t) do
    t |> Enum.map_join(" ", &replace_md_with_tag/1)
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Piping again and again

  defp replace_md_with_tag(w) do
    w
    |> replace_prefix_md
    |> replace_suffix_md
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Using string built in to avoid complexe regex
  # Also piping

  defp replace_prefix_md(w) do
    w
    |> String.replace_prefix("__", "<strong>")
    |> String.replace_prefix("_", "<em>")
  end

  # =============================================================
  # -------------------------------------------------------------
  ## 

  defp replace_suffix_md(w) do
    w
    |> String.replace_suffix("__", "</strong>")
    |> String.replace_suffix("_", "</em>")
  end

  # =============================================================
  # -------------------------------------------------------------
  ## Piping and Piping again and again and again...
  # Also removing some useless '<>' operator !

  defp unordered_tag(str) do
    str
    |> String.replace("<li>", "<ul><li>", global: false)
    |> String.replace_suffix("</li>", "</li></ul>")
  end
end
