defmodule Frequency do

  @doc """
  Count letter frequency in parallel.

  Returns a map of characters to frequencies.

  The number of worker processes to use can be set with 'workers'.
  """
  @spec frequency([String.t()], pos_integer) :: map
  def frequency([], _), do: %{}

  def frequency(texts, workers) do
    texts
    |> Enum.join # join the list as a super texts
    |> String.downcase
    |> String.replace(~r/[^\p{L}]/u, "") # cleaning unwanted characters
    |> launch(workers) # now is time to launch super texts through workers
  end

  def launch(text, workers) do
    text
    |> String.graphemes
    |> Enum.uniq # We are creating a list of all present char
    |> Enum.chunk_every(workers) # we chunk the list by worker
    |> Enum.map(&parallel_map(&1, text, workers)) # time to count in parallel
    |> Enum.concat
  end

  def parallel_map(list, text, workers) do
    1..workers
    |> Enum.reduce([], fn x, acc ->
      letter = Enum.at(list, x - 1)

      case letter do
        nil -> acc
        _ -> acc ++ [ Task.async(fn -> count_letter(text, letter) end) ]
      end
    end)
    |> Enum.map(&Task.await/1)
  end

  def count_letter(text, letter) do
    {:ok, reg} = Regex.compile("[^" <> letter <> "]", "iu")
    cnt = text |> String.replace(reg, "") |> String.length

    {letter, cnt}
  end
end
