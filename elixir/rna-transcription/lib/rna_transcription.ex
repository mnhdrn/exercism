defmodule RnaTranscription do

  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RnaTranscription.to_rna('ACTG')
  'UGAC'
  """

  @spec to_rna([char]) :: [char]
  def to_rna(dna) do
    Enum.map(dna, &transcript(&1))
  end

  # This function is just a staging function for to_rna/1
  @spec transcript(char) :: char
  defp transcript(c) do
      case c do
        ?A-> ?U
        ?C-> ?G
        ?T-> ?A
        ?G -> ?C
        _ -> c
      end
  end

end
