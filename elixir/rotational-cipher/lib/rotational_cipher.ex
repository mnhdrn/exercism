defmodule RotationalCipher do

  @upcase ?A..?Z
  @downcase ?a..?z

  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """

  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text 
    |> to_charlist
    |> Enum.map(&do_rot(&1, shift))
    |> to_string
  end

  @spec do_rot(char :: char, shift :: integer) :: char
  defp do_rot(char, shift) do
    case char do
      c when c in @upcase -> rem(char - ?A + shift, 26) + ?A
      c when c in @downcase -> rem(char - ?a + shift, 26) + ?a
      _ -> char
    end
  end

end
