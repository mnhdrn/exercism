defmodule RunLengthEncoder do

  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "AABBBCCCC" => "2A3B4C"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "2A3B4C" => "AABBBCCCC"
  """

  #------------------------------------ ENCODE
  #------------------------------------

  @spec encode(String.t()) :: String.t()
  def encode(string) do
    string
    |> to_charlist
    |> Enum.chunk_by(&(&1))
    |> Enum.map(&encode_process(&1))
    |> Enum.join
  end

  @spec encode_process(element :: [char]) :: String.t()
  defp encode_process(element) do
    count = Enum.count(element)
    value = to_string(element) |> String.at(0)

    if count > 1, do: to_string(count) <> value, else: value
  end

  #------------------------------------ DECODE
  #------------------------------------

  @spec decode(String.t()) :: String.t
  def decode(string) do
    Regex.scan(~r/(\d+)(.)|(.)/, string)
    |> Enum.map(fn(e) -> decode_process(e) end)
    |> Enum.join
  end

  @spec decode_process(List.t) :: String.t
  defp decode_process([_, _, _, c]) do
    String.duplicate(c, 1)
  end

  @spec decode_process(List.t) :: String.t
  defp decode_process([_, i, c]) do
    String.duplicate(c, String.to_integer(i))
  end

end
