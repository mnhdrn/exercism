defmodule Say do

  @hundred 100
  @thousand 1_000
  @million 1_000_000
  @billion 1_000_000_000
  @trillion 1_000_000_000_000

  100_000_000_000
  @range 0..@trillion - 1

  @num %{
    1 => "one", 2 => "two", 3 => "three", 4 => "four",
    5 => "five", 6 => "six", 7 => "seven", 8 => "eight",
    9 => "nine", 10 => "ten", 11 => "eleven", 12 => "twelve",
    13 => "thirteen", 14 => "fourteen", 15 => "fifteen", 16 => "sixteen",
    17 => "seventeen", 18 => "eighteen", 19 => "nineteen", 20 => "twenty",
    30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty",
    70 => "seventy", 80 => "eighty", 90 => "ninety"
  }

  @doc """
  Translate a positive integer into English.
  """
  @spec in_english(integer) :: {atom, String.t()}
  def in_english(number) when number not in @range do
    {:error, "number is out of range"}
  end

  def in_english(number) when number == 0, do: {:ok, "zero"}

  def in_english(number) do
    {:ok, words(number)}
  end

  def words(n) when n < 20, do: @num[n]

  def words(n) when n < @hundred do
    case rem(n, 10) do
      0 -> @num[n]
      rem -> @num[n - rem] <> "-" <> @num[rem]
    end
  end

  def words(n) when n < @thousand, do: huge(n, @hundred, "hundred")

  def words(n) when n < @million, do: huge(n, @thousand, "thousand")

  def words(n) when n < @billion, do: huge(n, @million, "million")

  def words(n) when n < @trillion, do: huge(n, @billion, "billion")

  def huge(n, section, suffix) do
    value = div(n, section)
    num = words(value) <> " " <> suffix

    case rem(n, section) do
      0 -> num
      rem -> num <> " " <> words(rem)
    end
  end


end

Say.in_english(1243)
|> IO.inspect
