defmodule Scrabble do

  @tab %{
    a: 1, e: 1, i: 1, o: 1, u: 1,
    l: 1, n: 1, r: 1, s: 1, t: 1,
    d: 2, g: 2,
    b: 3, c: 3, m: 3, p: 3,
    f: 4, h: 4, v: 4, w: 4, y: 4,
    k: 5,
    j: 8, x: 8,
    q: 10, z: 10,
  }

  @doc """
  Calculate the scrabble score for the word.
  """
  @spec score(String.t()) :: non_neg_integer
  def score(word) do
    word
    |> String.trim
    |> String.downcase
    |> encode
  end

  @spec encode(String.t()) :: String.t()
  def encode(string) do
    string
    |> to_charlist
    |> Enum.chunk_by(&(&1))
    |> Enum.reduce(0, &encode_process(&1, &2))
  end

  @spec encode_process(element :: [char], acc :: integer) :: String.t()
  defp encode_process(element, acc) do
    value = element |> Enum.slice(0, 1) |> List.to_atom
    count = Enum.count(element)

    acc + (@tab[value] * count)
  end

end
