defmodule SecretHandshake do
  use Bitwise

  @spec commands(code :: integer) :: list(String.t())
  @spec process(bin :: integer, str :: String.t, code :: integer) :: List.t
  @spec reverse(lst :: List.t, code :: integer) :: List.t

  @result [
    {0b1, "wink"},
    {0b10, "double blink"},
    {0b100, "close your eyes"},
    {0b1000, "jump"},
  ]

  @doc """
  Determine the actions of a secret handshake based on the binary
  representation of the given `code`.

  If the following bits are set, include the corresponding action in your list
  of commands, in order from lowest to highest.

  1 = wink
  10 = double blink
  100 = close your eyes
  1000 = jump

  10000 = Reverse the order of the operations in the secret handshake
  """

  def commands(code) when is_integer(code) do
    Enum.reduce(@result, [], fn ({bin, str}, acc) ->
      acc ++ process(bin, str, code)
    end)
    |> reverse(code)
  end

  defp process(bin, str, code) do
    if (code &&& bin) != 0, do: [str], else: []
  end

  defp reverse(lst, code) do
    if (code &&& 0b10000) != 0 do
      lst |> Enum.reverse
    else
      lst
    end
  end

end
