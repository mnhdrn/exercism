defmodule SimpleCipher do
  @doc """
  Given a `plaintext` and `key`, encode each character of the `plaintext` by
  shifting it by the corresponding letter in the alphabet shifted by the number
  of letters represented by the `key` character, repeating the `key` if it is
  shorter than the `plaintext`.

  For example, for the letter 'd', the alphabet is rotated to become:

  defghijklmnopqrstuvwxyzabc

  You would encode the `plaintext` by taking the current letter and mapping it
  to the letter in the same position in this rotated alphabet.

  abcdefghijklmnopqrstuvwxyz
  defghijklmnopqrstuvwxyzabc

  "a" becomes "d", "t" becomes "w", etc...

  Each letter in the `plaintext` will be encoded with the alphabet of the `key`
  character in the same position. If the `key` is shorter than the `plaintext`,
  repeat the `key`.

  Example:

  plaintext = "testing"
  key = "abc"

  The key should repeat to become the same length as the text, becoming
  "abcabca". If the key is longer than the text, only use as many letters of it
  as are necessary.
  """

  def encode(plaintext, key) do
    source = plaintext |> prepare
    encryption = key |> prepare(plaintext)

    source
    |> Enum.zip(encryption)
    |> process(&calcul_encode/2)
    |> List.to_string
  end

  @doc """
  Given a `ciphertext` and `key`, decode each character of the `ciphertext` by
  finding the corresponding letter in the alphabet shifted by the number of
  letters represented by the `key` character, repeating the `key` if it is
  shorter than the `ciphertext`.

  The same rules for key length and shifted alphabets apply as in `encode/2`,
  but you will go the opposite way, so "d" becomes "a", "w" becomes "t",
  etc..., depending on how much you shift the alphabet.
  """

  def decode(ciphertext, key) do
    source = ciphertext |> prepare
    encryption = key |> prepare(ciphertext)

    source
    |> Enum.zip(encryption)
    |> process(&calcul_decode/2)
    |> List.to_string
  end

  #=======================================
  # - Calcul for the Cipher

  defp calcul_encode(char, key), do: ?a + rem(char + key - ?a * 2, 26)

  defp calcul_decode(char, key), do: ?a + rem(26 + char - key, 26)

  #=======================================
  # - Process the string

  defp process(lst, op) do
    lst
    |> Enum.map(fn {x, y} ->
      case x in ?a..?z do
        true -> op.(x, y)
        _ -> x
      end
    end)
  end

  #=======================================
  # - Prepare the strings

  defp prepare(str) do
    str
    |> String.graphemes
    |> List.to_charlist
  end

  defp prepare(str, text) when byte_size(str) < byte_size(text) do
    str
    |> String.duplicate(String.length(text))
    |> String.graphemes
    |> List.to_charlist
  end

  defp prepare(str, text) when byte_size(str) > byte_size(text) do
    str
    |> String.graphemes
    |> List.to_charlist
    |> Enum.slice(0..String.length(text))
  end

  defp prepare(str, _text) do
    str
    |> String.graphemes
    |> List.to_charlist
  end
end
