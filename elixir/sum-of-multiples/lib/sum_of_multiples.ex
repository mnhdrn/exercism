defmodule SumOfMultiples do
  @doc """
  Adds up all numbers from 1 to a given end number that are multiples of the factors provided.
  """

  @spec to(non_neg_integer, [non_neg_integer]) :: non_neg_integer
  def to(limit, factors) do
    1..(limit - 1)
    |> Enum.filter(&to_multiple(&1, factors))
    |> Enum.sum
  end

  @spec is_factor(non_neg_integer, [non_neg_integer]) :: boolean()
  defp to_multiple(num, factors) do
    Enum.any?(factors, &is_factor(num, &1))
  end

  @spec is_factor(non_neg_integer, non_neg_integer) :: boolean()
  defp is_factor(num, factor) do
    rem(num, factor) == 0
  end

end

