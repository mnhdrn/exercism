defmodule Tournament do

  defstruct match: 0, win: 0, draw: 0, loss: 0, point: 0

  @head "Team                           | MP |  W |  D |  L |  P"


  @doc """
  Given `input` lines representing two teams and whether the first of them won,
  lost, or reached a draw, separated by semicolons, calculate the statistics
  for each team's number of games played, won, draw, lost, and total points
  for the season, and return a nicely-formatted string table.

  A win earns a team 3 points, a draw earns 1 point, and a loss earns nothing.

  Order the outcome by most total points for the season, and settle ties by
  listing the teams in alphabetical order.
  """
  @spec tally(input :: list(String.t())) :: String.t()
  def tally(input) do
    input
    |> prepare # will reject invalid and stock valid to map, before returning a list
    |> Enum.sort_by(fn {k, v} -> String.first(k) && v.point end, &Kernel.>=/2) #we sort by score and then name
    |> Enum.reduce([@head], fn({key, val}, acc) -> # building the final string with format/2
      acc ++ [ format(key, val) ]
    end)
    |> Enum.join("\n") # magic.
  end

  defp prepare(input) do
    input
    |> Enum.filter(fn x -> 
      Regex.match?(~R/.*;.*;(win|loss|draw)$/, x) # remove unvalid line
    end)
    |> Enum.reduce(%{}, &(process(&1, &2))) # Parse the line and stock in a map
    |> Map.to_list
    |> Enum.map(fn {k, v } ->
      {Atom.to_string(k), v}
    end)
  end

  # ==========================================
  defp format(key, val) do
    title = String.pad_trailing("#{key}", 30)
    match = String.pad_leading("#{val.match}", 2)
    win = String.pad_leading("#{val.win}", 2)
    loss = String.pad_leading("#{val.loss}", 2)
    draw = String.pad_leading("#{val.draw}", 2)
    point = String.pad_leading("#{val.point}", 2)


    [title, match, win, draw, loss, point] |> Enum.join(" | ")
  end

  # ==========================================
  defp process(input, acc) do
    [t1, t2, score] = String.split(input, ";")
    game = get_score(score)
    ret =  insert(game, t1, acc)

    case game do
      {1, 0, 0, 3} -> insert({0, 0, 1, 0}, t2, ret) # t1 win - t2 loss
      {0, 1, 0, 1} -> insert({0, 1, 0, 1}, t2, ret) # draw
      {0, 0, 1, 0} -> insert({1, 0, 0, 3}, t2, ret) # t1 loss - t2 win
      _ -> insert({0, 0, 0, 0}, t2, ret)
      end
  end

  # ==========================================
  defp get_score(score) do
    case score do
      "win" -> {1, 0, 0, 3}
      "draw" -> {0, 1, 0, 1}
      "loss" -> {0, 0, 1, 0}
      _ -> {0, 0, 0, 0}
    end
  end

  # ==========================================
  defp insert({w, d, l, p}, team, acc) do
    key = String.to_atom(team)
    val = acc[key] || %__MODULE__{}

    ret = %__MODULE__{
      match: val.match + 1,
      win: val.win + w,
      draw: val.draw + d,
      loss: val.loss + l,
      point: val.point + p
    }

    acc |> update(team, ret)
  end

  # ==========================================
  defp update(acc, team, struct) do
    key = String.to_atom(team)

    Map.put(acc, key, struct)
  end
end
