defmodule Triangle do
  @type kind :: :equilateral | :isosceles | :scalene

  @doc """
  Return the kind of triangle of a triangle with 'a', 'b' and 'c' as lengths.
  """
  @spec kind(number, number, number) :: {:ok, kind} | {:error, String.t()}

  def kind(a, b, c) when a <= 0  or b <= 0 or c <= 0, do: {:error, "all side lengths must be positive"}

  def kind(a, b, c) do
    cond do
      is_ine?(a, b, c) -> {:error, "side lengths violate triangle inequality"}
      is_equi?(a, b ,c) -> {:ok, :equilateral}
      is_iso?(a, b, c) -> {:ok, :isosceles}
      true -> {:ok, :scalene}
    end
  end

  defp is_equi?(a, b, c) do
    a == b and b == c and c == a
  end

  defp is_iso?(a, b, c) do
    a == b or b == c or c == a
  end

  defp is_ine?(a, b, c) do
    (a + b) <= c
    or (b + c) <= a
    or (a + c) <= b
  end

end
