defmodule Words do

  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """

  #### count/1
  # First we downcase our sentence
  # then we split to a list with a certain regex:
  #    [^ -> begin by [:alnum:] -> alpha num \- -> keep hyphens] \u -> keep unicode
  # We use the trim: true, to remove "" in the list.
  # then give the list in reduce.
  # It was not easy to understand that Map.update create an key if doesn't exist
  ####

  @spec count(String.t()) :: map
  def count(sentence) when is_binary(sentence) do
    IO.inspect sentence
    |> String.downcase
    |> String.split(~R/[^[:alnum:]\-]/u, trim: true)
    |> Enum.reduce(Map.new, fn(word, acc) ->
      Map.update(acc, word, 1, &(&1 + 1))
    end)
  end

end
