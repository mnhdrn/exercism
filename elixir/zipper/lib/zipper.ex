defmodule Zipper do
  import BinTree

  defstruct record: nil, trail: []

  alias __MODULE__, as: Z

  # ==========================================================
  # ----------------------------------------------------------
  @spec from_tree(BinTree.t()) :: Zipper.t()
  @doc """
  Get a zipper focused on the root node.
  """

  def from_tree(bin_tree) do
    %Z{record: bin_tree}
  end

  # ==========================================================
  # ----------------------------------------------------------

  @spec to_tree(Zipper.t()) :: BinTree.t()
  @doc """
  Get the complete tree from a zipper.
  """

  def to_tree(%Z{trail: []} = zipper), do: zipper.record

  def to_tree(zipper), do: zipper |> up |> to_tree

  # ==========================================================
  # ----------------------------------------------------------

  @spec value(Zipper.t()) :: any
  @doc """
  Get the value of the focus node.
  """

  def value(zipper), do: zipper.record.value

  # ==========================================================
  # ----------------------------------------------------------

  @spec left(Zipper.t()) :: Zipper.t() | nil
  @doc """
  Get the left child of the focus node, if any.
  """

  def left(%Z{record: %BinTree{left: nil}}), do: nil

  def left(zipper) do
    %Z{
      record: zipper.record.left,
      trail: [
        {:left, zipper.record.value, zipper.record.right}
        | zipper.trail
      ]
    }
  end

  # ==========================================================
  # ----------------------------------------------------------

  @spec right(Zipper.t()) :: Zipper.t() | nil
  @doc """
  Get the right child of the focus node, if any.
  """

  def right(%Z{record: %BinTree{right: nil}}), do: nil

  def right(zipper) do
    %Z{
      record: zipper.record.right,
      trail: [
        {:right, zipper.record.value, zipper.record.left}
        | zipper.trail
      ]
    }
  end

  # ==========================================================
  # ----------------------------------------------------------

  @spec up(Zipper.t()) :: Zipper.t() | nil
  @doc """
  Get the parent of the focus node, if any.
  """

  def up(%Z{trail: []}), do: nil

  def up(%Z{trail: [{:left, value, right} | td]} = zipper) do
    %Z{
      record: %BinTree{value: value, left: zipper.record, right: right},
      trail: td
    }
  end

  def up(%Z{trail: [{:right, value, left} | td]} = zipper) do
    %Z{
      record: %BinTree{value: value, left: left, right: zipper.record},
      trail: td
    }
  end

  # ==========================================================
  # ----------------------------------------------------------

  @spec set_value(Zipper.t(), any) :: Zipper.t()
  @doc """
  Set the value of the focus node.
  """

  def set_value(zipper, value) do
    %Z{zipper | record: %BinTree{zipper.record | value: value}}
  end

  # ==========================================================
  # ----------------------------------------------------------

  @spec set_left(Zipper.t(), BinTree.t() | nil) :: Zipper.t()
  @doc """
  Replace the left child tree of the focus node.
  """

  def set_left(zipper, left) do
    %Z{zipper | record: %BinTree{zipper.record | left: left}}
  end

  # ==========================================================
  # ----------------------------------------------------------

  @spec set_right(Zipper.t(), BinTree.t() | nil) :: Zipper.t()
  @doc """
  Replace the right child tree of the focus node.
  """

  def set_right(zipper, right) do
    %Z{zipper | record: %BinTree{zipper.record | right: right}}
  end
end
