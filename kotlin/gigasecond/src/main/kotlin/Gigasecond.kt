import java.time.LocalDate
import java.time.LocalDateTime

class Gigasecond(sourceDateTime: LocalDateTime) {

    constructor(sourceTime: LocalDate) : this(sourceTime.atStartOfDay())

    val date: LocalDateTime = sourceDateTime.plusSeconds(1_000_000_000)
}
