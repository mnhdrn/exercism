object Raindrops {

    private val factors = listOf(3,5,7)

    fun convert(n: Int): String {
        val ret = (1..n)
            .filter({ n % it == 0 })
            .filter({ it in factors })
            .map({ process(it) })
            .joinToString("")

        if (ret == "") {
            return n.toString()
        }

        return ret
    }

    private fun process(n : Int) : String {
        if (n % 3 == 0) { return "Pling" }
        if (n % 5 == 0) { return "Plang" }
        if (n % 7 == 0) { return "Plong" }
        return ""
    }
}
