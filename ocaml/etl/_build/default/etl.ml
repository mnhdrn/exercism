let process n lst =
  List.map (fun x -> (Char.lowercase_ascii x, n)) lst

let transform old =
  old
  |> List.map (fun (x, y) -> process x y)
  |> List.flatten
  |> List.sort (fun (x, _) (y, _) -> Char.compare x y)
