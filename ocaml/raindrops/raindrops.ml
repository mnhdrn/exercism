let raindrop n =
  let factors = [(3, "Pling"); (5, "Plang");(7, "Plong")] in
  let ret =
    factors
    |> List.filter (fun (x, _) -> (n mod x) == 0)
    |> List.fold_left (fun acc (_, y) -> acc ^ y) ""
  in
  match ret with
  | "" -> Int.to_string n
  | ret -> ret
