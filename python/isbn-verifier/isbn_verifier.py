import re

def is_valid(isbn):
  isbn = isbn.replace('-', '')
  if len(isbn) != 10 or not re.match(r'\d{9}(\d|X)$', isbn):
    return False
  return calculate(isbn)

def transform(isbn):
  isbn = list(isbn)
  if isbn[-1] == 'X':
    isbn[-1] = '10'
  return [int(n) for n in isbn]

def calculate(isbn):
  index = zip(range(0, 11), range(10, 0, -1))
  lst = transform(isbn)
  return sum([lst[i] * j for (i, j) in index]) % 11 == 0
