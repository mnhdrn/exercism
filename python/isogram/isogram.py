def is_isogram(string):
  lst = sorted([i for i in string.lower() if i != " " and i != "-"])
  return len(lst) == len(set(lst))
