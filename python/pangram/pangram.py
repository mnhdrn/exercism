def is_pangram(sentence):
    # generate alpha, and set it
    alphabet = set(list(map(chr, range(97, 123)))) 

    # remove space, set string to lowercase, split it, sorted it, set it
    lst = set(sorted(list(sentence.lower().replace(" ","")))) # lower, remove space

    # get the diff between the two list, if empty it mean is a pangram
    diff = list(alphabet - lst)
    return True if not diff else False
