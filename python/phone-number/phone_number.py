import re

class PhoneNumber:
    def __init__(self, number):
      self.number = re.sub(r"^[\+1]|[\.\s\-\(\)]", "", number)
      self.number = self.validate(self.number)
      self.area_code = self.number[:3]
      self.exchange_code = self.number[3:6]
      self.suscriber_num= self.number[6:]

    def validate(self, number):
      length = len(number)

      if length < 10 or length > 11:
        raise ValueError("Wrong Number Length")

      if length == 11:
        if number[0] != '1':
          raise ValueError("Incorrect country code")
        else:
          number = number[1:]

      if re.findall("[A-z]", number):
        raise ValueError("Incorrect Number: alpha inside")

      if int(number[0]) <= 1 or int(number[3]) <= 1:
        raise ValueError("Incorrect Number")

      return number

    def pretty(self):
      return f"({self.area_code})-{self.exchange_code}-{self.suscriber_num}"


# print(PhoneNumber("(223) 456-7890").number)
# print(PhoneNumber("(223) 456-7890").area_code)
# print(PhoneNumber("(223) 45a-7890").number)
