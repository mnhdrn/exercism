def factors(value):
  factors = 2
  ret = []
  while value != 1:
    if value % factors == 0:
      ret.append(factors)
      value = value / factors
    else:
      factors += 1
  return ret

