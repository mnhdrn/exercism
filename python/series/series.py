def slices(series, length):
  if length > len(series) or length < 1:
    raise ValueError("wrong length number")

  lst = []
  for i in range(0, len(series)):
    lst.append(series[i:(i + length)])

  return [n for n in lst if len(n) == length]
